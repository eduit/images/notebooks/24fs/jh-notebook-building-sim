FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-universal-ethz:3.0.0-08

USER root

RUN git clone -q https://github.com/santoshphilip/eppy.git && \
  cd eppy && \
  python3 -m setup install

RUN wget https://github.com/NREL/EnergyPlus/releases/download/v24.1.0/EnergyPlus-24.1.0-9d7789a3ac-Linux-Ubuntu20.04-x86_64.sh && \
  chmod a+x EnergyPlus-24.1.0-9d7789a3ac-Linux-Ubuntu20.04-x86_64.sh && \
  sh -c "echo 'y\n\n' | ./EnergyPlus-24.1.0-9d7789a3ac-Linux-Ubuntu20.04-x86_64.sh" 

USER 1000
